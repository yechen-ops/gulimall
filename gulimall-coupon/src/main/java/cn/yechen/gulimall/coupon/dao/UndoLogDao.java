package cn.yechen.gulimall.coupon.dao;

import cn.yechen.gulimall.coupon.entity.UndoLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author yechen
 * @email 15715830811@163.com
 * @date 2021-09-25 13:47:25
 */
@Mapper
public interface UndoLogDao extends BaseMapper<UndoLogEntity> {
	
}
