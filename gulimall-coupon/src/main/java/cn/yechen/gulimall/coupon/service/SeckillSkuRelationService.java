package cn.yechen.gulimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.yechen.gulimall.common.utils.PageUtils;
import cn.yechen.gulimall.coupon.entity.SeckillSkuRelationEntity;

import java.util.Map;

/**
 * 秒杀活动商品关联
 *
 * @author yechen
 * @email 15715830811@163.com
 * @date 2021-09-25 13:47:25
 */
public interface SeckillSkuRelationService extends IService<SeckillSkuRelationEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

