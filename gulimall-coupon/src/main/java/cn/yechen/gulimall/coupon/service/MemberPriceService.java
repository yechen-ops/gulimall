package cn.yechen.gulimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.yechen.gulimall.common.utils.PageUtils;
import cn.yechen.gulimall.coupon.entity.MemberPriceEntity;

import java.util.Map;

/**
 * 商品会员价格
 *
 * @author yechen
 * @email 15715830811@163.com
 * @date 2021-09-25 13:47:25
 */
public interface MemberPriceService extends IService<MemberPriceEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

