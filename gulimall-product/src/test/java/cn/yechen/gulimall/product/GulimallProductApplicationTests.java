package cn.yechen.gulimall.product;

import cn.yechen.gulimall.product.entity.BrandEntity;
import cn.yechen.gulimall.product.service.BrandService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.conditions.query.QueryChainWrapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class GulimallProductApplicationTests {

	@Autowired
	private BrandService brandService;

	@Test
	void contextLoads() {
		// 保存
		/*BrandEntity brandEntity = new BrandEntity();
		brandEntity.setDescript("中华有为");
		brandEntity.setName("华为");
		boolean save = brandService.save(brandEntity);
		if (save) {
			System.out.println("保存成功");
		}*/
		// 查询
		QueryWrapper<BrandEntity> brandEntityQueryWrapper = new QueryWrapper<>();
		List<BrandEntity> list = brandService.list(brandEntityQueryWrapper.eq("brand_id", 1L));
		list.forEach(System.out::println);
	}

}
