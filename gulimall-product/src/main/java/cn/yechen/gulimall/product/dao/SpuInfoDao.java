package cn.yechen.gulimall.product.dao;

import cn.yechen.gulimall.product.entity.SpuInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu信息
 * 
 * @author yechen
 * @email 15715830811@163.com
 * @date 2021-09-24 23:43:18
 */
@Mapper
public interface SpuInfoDao extends BaseMapper<SpuInfoEntity> {
	
}
