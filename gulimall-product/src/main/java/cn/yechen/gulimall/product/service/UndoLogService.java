package cn.yechen.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.yechen.gulimall.common.utils.PageUtils;
import cn.yechen.gulimall.product.entity.UndoLogEntity;

import java.util.Map;

/**
 * 
 *
 * @author yechen
 * @email 15715830811@163.com
 * @date 2021-09-24 23:43:18
 */
public interface UndoLogService extends IService<UndoLogEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

