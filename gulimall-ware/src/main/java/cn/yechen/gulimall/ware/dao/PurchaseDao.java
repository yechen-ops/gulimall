package cn.yechen.gulimall.ware.dao;

import cn.yechen.gulimall.ware.entity.PurchaseEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 采购信息
 * 
 * @author yechen
 * @email 15715830811@163.com
 * @date 2021-09-25 14:08:56
 */
@Mapper
public interface PurchaseDao extends BaseMapper<PurchaseEntity> {
	
}
