package cn.yechen.gulimall.ware.dao;

import cn.yechen.gulimall.ware.entity.WareInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 仓库信息
 * 
 * @author yechen
 * @email 15715830811@163.com
 * @date 2021-09-25 14:08:56
 */
@Mapper
public interface WareInfoDao extends BaseMapper<WareInfoEntity> {
	
}
