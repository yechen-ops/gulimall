package cn.yechen.gulimall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.yechen.gulimall.common.utils.PageUtils;
import cn.yechen.gulimall.order.entity.RefundInfoEntity;

import java.util.Map;

/**
 * 退款信息
 *
 * @author yechen
 * @email 15715830811@163.com
 * @date 2021-09-25 14:03:01
 */
public interface RefundInfoService extends IService<RefundInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

