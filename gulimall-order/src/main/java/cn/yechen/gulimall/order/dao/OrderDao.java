package cn.yechen.gulimall.order.dao;

import cn.yechen.gulimall.order.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单
 * 
 * @author yechen
 * @email 15715830811@163.com
 * @date 2021-09-25 14:03:01
 */
@Mapper
public interface OrderDao extends BaseMapper<OrderEntity> {
	
}
