package cn.yechen.gulimall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.yechen.gulimall.common.utils.PageUtils;
import cn.yechen.gulimall.member.entity.UndoLogEntity;

import java.util.Map;

/**
 * 
 *
 * @author yechen
 * @email 15715830811@163.com
 * @date 2021-09-25 13:57:34
 */
public interface UndoLogService extends IService<UndoLogEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

