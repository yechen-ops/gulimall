package cn.yechen.gulimall.member.dao;

import cn.yechen.gulimall.member.entity.UndoLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author yechen
 * @email 15715830811@163.com
 * @date 2021-09-25 13:57:34
 */
@Mapper
public interface UndoLogDao extends BaseMapper<UndoLogEntity> {
	
}
