package cn.yechen.gulimall.member.dao;

import cn.yechen.gulimall.member.entity.MemberEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员
 * 
 * @author yechen
 * @email 15715830811@163.com
 * @date 2021-09-25 13:57:33
 */
@Mapper
public interface MemberDao extends BaseMapper<MemberEntity> {
	
}
